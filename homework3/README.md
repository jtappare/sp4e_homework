# Homework: External libraries: application to a heat equation solver

## Usage

- clone submodules
  -  `git submodule update --init`
- create a build folder and move inside
  -  `mkdir build`
  -  `cd build`
- build
  -  `cmake .. -DUSE_FFTW=<ON/OFF>`
  -  `make`
- run tests
  -  `make test`
- (Opt.) generate and open the doxygen documentation
  -  `make doc`
- (Opt.) Open documentation in a browser (e.g. firefox)
  -  `firefox <path_to_repo>/sp4e_homework/homework3/build/html/index.html`
-  create the dumps folder
   -  `mkdir dumps`
- Launch the main script using:
	- `./particles nsteps dump_freq input.csv material_point timestep`
	- Don't forget to create the dumps folder before running the first time  
  
## Dependencies

### Python

- python3
- numpy
- csv

### C++

- CMake
- gcc
- fftw3 (for material points simulations)

## structure

- <details><summary>src/</summary>
    - googletest<br>
    - CMakeLists.txt<br>
	- compute_boundary.cc <br>
	- compute_boundary.hh <br>
	- compute_contact.cc <br>
	- compute_contact.hh<br>
	- compute_energy.cc<br>
	- compute_energy.hh<br>
	- compute_gravity.cc<br>
	- compute_gravity.hh<br>
	- compute_interaction.cc<br>
	- compute_interaction.hh<br>
	- compute_kinetic_energy.cc<br>
	- compute_kinetic_energy.hh<br>
	- compute_potential_energy.cc<br>
	- compute_potential_energy.hh<br>
	- compute_temperature.cc<br>
	- compute_temperature.hh<br>
	- compute_verlet_integration.cc<br>
	- compute_verlet_integration.hh<br>
	- compute.hh<br>
	- csv_reader.cc<br>
	- csv_reader.hh<br>
	- csv_writer.cc<br>
	- csv_writer.hh<br>
	- fft.hh<br>
	- main.cc<br>
	- material_point.cc<br>
	- material_point.hh<br>
	- material_points_factory.cc<br>
	- material_points_factory.hh<br>
	- matrix.hh<br>
	- my_types.hh<br>
	- particles.cc<br>
	- particles.hh<br>
	- particles_factory_interface.cc<br>
	- particles_factory_interface.hh<br>
	- ping_pong_ball.cc<br>
	- ping_pong_ball.hh<br>
	- ping_pong_ball_factory.cc<br>
	- ping_pong_ball_factory.hh<br>
	- planet.cc<br>
	- planet.hh<br>
	- planets_factory.cc<br>
	- planets_factory.hh<br>
	- system_evolution.cc<br>
	- system_evolution.hh<br>
	- system.cc<br>
	- system.hh<br>
	- test_fft.cc<br>
	- test_kepler.cc<br>
	- test_temperature.cc<br>
	- vector_inline.hh<br>
	- vector.hh<br>
</details>

- CMakeLists.txt
- .gitignore
- README.md
- generate_temp_case.py
- paraview_temp.psvm

## Questions

### Question 1: describe how the particles are organized in the various objects.

There is a new particle type: `MaterialPoint`. The class inherits from `Particle`. It has originally two additional parameters, temperature and heat rate, and we have also added heat source. 
As for the other type of particles, `MaterialPoint` has a factory class that also inherits from `ParticlesFactoryInterface`. The `createSimulation` function calls `ComputeTemperature` which solves the heat equation on a square grid by calling `FFT`.

The structure `Matrix` is used to interact with square matrices. The FFT transforms are performed on the matrices of this structure. 

The `FFT` structure is used to compute the forward and backward FFT as well as the frequencies. 

### Question 5: boundary condition
We impose a zero boundary condition to the domain if the boolean parameter `null_boder` is true. It is set using the function `setNullBorder` of `ComputeTemperature`. 

In the function `compute`of `ComputeTemperature`, if the cell is on the edge of the domain, we set its temperature to zero instead of updating the temperature with the explicit scheme.

### Question 6:
- move inside the build folder
- generate the input particle file
  - `python ../generate_temp_case.py`
- Run the simulation 
  - e.g. `./particles 10000 500 temp_input.csv material_point 1e-6`
  - (opt) Take the time of the simulation to appreciate our heat diffusion GIFs below
- Open Paraview
- If the proposed simulation was used:
  - In File>Load state select <path_to_this_repo>/homework3/paraview_temp.psvm
- Else
  - File > open and select <path_to_this_repo>/homework3/build/dumps/step-..csv
  - In properties untick "Have headers" and set "Field delimiter" to " "(a space)
  - Press apply
  - In Filters>Alphabetical select "TableToPoints"
  - For  "X,Y, and Z Column" select "Field 0,1, and 3" respectively
  - In Filters>Common>Glyphs
  - set "Glyph Type" to "2D Glyph" and "square"
  - Tick "Filled"
  - Set "Masking>Glyph Mode" to "All points"
  - Set "Scale factor" to <dx used for the grid> (0.0078125 in our example)
  - In "Coloring" choose "Field 13"
  - Set range between 0 and 0.01 (third button right from "Edit")
  - Select Apply
## GIFs
  <img src="https://gitlab.epfl.ch/jtappare/gifs/-/raw/main/sp4e.gif" width="300" height="300" />
  <img src="  https://gitlab.epfl.ch/jtappare/gifs/-/raw/main/circ.gif" width="300" height="300" />
  <img src="https://gitlab.epfl.ch/jtappare/gifs/-/raw/main/tux.gif" width="300" height="300" />
  <img src="  https://gitlab.epfl.ch/jtappare/gifs/-/raw/main/gui.gif" width="300" height="300" />
  
## Authors
- Roxane Ferry
- Joachim Tapparel
