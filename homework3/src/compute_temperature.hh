#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__
#include <iomanip>
/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "matrix.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {
  Real dt;
  Real dx = 1.0;
  Real rho = 1.0;
  Real spe_heat_capacity = 1.0;
  bool null_border = false;

  std::vector<double> range;
  bool heat_src_fft_done=false;
  Matrix<std::complex<double>> heat_src_fft;

  // Virtual implementation
public:
  //! Penalty contact implementation
  void compute(System& system) override;
  void setDeltaT(Real dt);
  void setMassDensity(Real rho);
  void setNullBorder(bool null_border);
  void setSpecificHeatCapacity(Real spe_heat_capacity);
  void initRangeFromParticles(System &system);

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
