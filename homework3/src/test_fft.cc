#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);    
    // if (std::abs(val) > 1e-10)
    //   std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  Matrix<complex> res2 = FFT::itransform(res);

  for (auto&& entry : index(res2)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);    
    ASSERT_NEAR(val.real(), cos(k*i), 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, wave_numbers) {
  UInt N = 7;
  Matrix<std::complex<double>> res_odd = FFT::computeFrequencies(N);
  
  for (auto&& entry : index(res_odd)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);    
    if (j == 0) 
      ASSERT_EQ(val.real(), 0);
    else if (j == (N - 1))
      ASSERT_NEAR(val.real(), -1, 1e-10);
    else if (j == (N - 1) / 2)
      ASSERT_NEAR(val.real(), 3, 1e-10);
    else if (j == ((N - 1) / 2 +1))
      ASSERT_NEAR(val.real(), -3, 1e-15);
  } 

  UInt N2 = 6;
  Matrix<std::complex<double>> res_even = FFT::computeFrequencies(N2);
  
  for (auto&& entry : index(res_even)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);    
    if (j == 0) 
      ASSERT_EQ(val.real(), 0);
    else if (j == (N2 - 1))
      ASSERT_NEAR(val.real(), -1, 1e-10);
    else if (j == N2 / 2 - 1)
      ASSERT_NEAR(val.real(), 2, 1e-10);
    else if (j == (N2 / 2))
      ASSERT_NEAR(val.real(), -3, 1e-10);
  } 
}
/*****************************************************************/
