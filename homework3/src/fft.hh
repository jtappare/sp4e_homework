#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT
{

  static Matrix<complex> transform(Matrix<complex> &m);
  static Matrix<complex> itransform(Matrix<complex> &m);

  static Matrix<std::complex<double>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex> &m_in)
{
  std::vector<std::complex<double> > in(m_in.size()*m_in.size()), out(m_in.size()*m_in.size());
  fftw_plan p;
  
  for (auto&& entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    UInt k = i+j*m_in.size();
    in[k] = val;
  }

  p = fftw_plan_dft_2d( m_in.size(),
	                      m_in.size(),
	                      reinterpret_cast<fftw_complex*>(&in[0]),
	                      reinterpret_cast<fftw_complex*>(&out[0]),
	                      FFTW_FORWARD,
	                      FFTW_ESTIMATE );
  fftw_execute(p); 

  Matrix<complex> res(m_in.size());
  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    UInt k = i+j*m_in.size();
    val = out[k];
  }

  fftw_destroy_plan(p);
  fftw_cleanup();

  return res;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex> &m_in) 
{
  std::vector<std::complex<double> > in(m_in.size()*m_in.size()), out(m_in.size()*m_in.size());
  fftw_plan p;

  for (auto&& entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    UInt k = i+j*m_in.size();
    in[k] = val;
  }

  p = fftw_plan_dft_2d( m_in.size(),
	                      m_in.size(),
	                      reinterpret_cast<fftw_complex*>(&in[0]),
	                      reinterpret_cast<fftw_complex*>(&out[0]),
	                      FFTW_BACKWARD,
	                      FFTW_ESTIMATE );
  fftw_execute(p); 

  Matrix<complex> res(m_in.size());
  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    UInt k = i+j*m_in.size();
    val = out[k];
  }

  fftw_destroy_plan(p);
  fftw_cleanup();

  return res /= (m_in.size()*m_in.size());
}

/* ------------------------------------------------------ */
inline Matrix<std::complex<double>> FFT::computeFrequencies(int size){
  Matrix<std::complex<double>> wavenumbers(size);

  if (size % 2) { // if size is odd
    for (auto&& entry : index(wavenumbers)) {
      int i = std::get<0>(entry);
      int j = std::get<1>(entry);
      auto& val = std::get<2>(entry);
      if (j <= ((size - 1) / 2)) {
        val = j;
      }
      else {
        val = (j - size);
      }
    }
  }
  else {
    for (auto&& entry : index(wavenumbers)) {
      int i = std::get<0>(entry);
      int j = std::get<1>(entry);
      auto& val = std::get<2>(entry);
      if (j <= (size / 2 - 1)) {
        val = (j);
      }
      else {
        val = (j - size);
      }
    }
  }

  return wavenumbers;
}

#endif // FFT_HH
