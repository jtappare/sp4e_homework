#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "system.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include <gtest/gtest.h>

// Fixture class
class TestTemp : public ::testing::Test
{
protected:
  void SetUp() override
  {
    temperature = std::make_shared<ComputeTemperature>();
  }

  System system;
  std::shared_ptr<ComputeTemperature> temperature;
};

// /*****************************************************************/
TEST_F(TestTemp, homgeneous_temp)
{
  int gridSize = 128;
  double init_temp = 5.6;
  Matrix<std::complex<double>> heatSrc(gridSize);
  for (int i = 0; i < gridSize; i++)
  {
    for (int j = 0; j < gridSize; j++)
    {
      MaterialPoint p;
      p.getTemperature() = init_temp;
      p.getHeatSource() = 0.0;
      p.getHeatRate() = 1.0;
      p.getPosition()[0] = i;
      p.getPosition()[1] = j;
      system.addParticle(std::make_shared<MaterialPoint>(p));

      heatSrc(i, j) = std::complex<double>(0.0);
    }
  }
  temperature->initRangeFromParticles(system);
  temperature->compute(system);

  for (auto &par : system)
  {
    ASSERT_NEAR(static_cast<MaterialPoint &>(par).getTemperature(), init_temp, 1e-15);
  }
}

/*****************************************************************/
TEST_F(TestTemp, equilibrium_with_heat_src)
{
  
  int mat_size = 128;
  std::vector<double> range = {-1.0, 1.0};
  int L = range[1]-range[0];

  Matrix<std::complex<double>> heatSrc(mat_size);
  int idx_x = 0;
  int idx_y = 0;
  for (float x = range[0]; x < range[1]; x += (range[1] - range[0]) / (mat_size ))
  {
    idx_y = 0;
    for (float y = range[0]; y < range[1]; y += (range[1] - range[0]) / (mat_size ))
    {
      MaterialPoint p;
      p.getTemperature() = sin(2.0 * M_PI * x / L);
      p.getHeatRate() = 1.0;
      p.getHeatSource() = (2.0 * M_PI / L) * (2.0 * M_PI / L) * sin(2.0 * M_PI * x / L);
      p.getPosition()[0] = x;
      p.getPosition()[1] = y;
      system.addParticle(std::make_shared<MaterialPoint>(p));
      idx_y++;
    }
    idx_x++;
  }

  temperature->setDeltaT(1e-8);
  temperature->setNullBorder(false);
  temperature->initRangeFromParticles(system);
  for (int i = 0; i < 100; i++)
  {
    temperature->compute(system);
  }

  for (auto &par : system)
  {
    double x = par.getPosition()[0];
    double equi_temp = sin(2.0 * M_PI * x / L);
    ASSERT_NEAR(static_cast<MaterialPoint &>(par).getTemperature(), equi_temp, 1e-6);
  }
}
/*****************************************************************/
TEST_F(TestTemp, equilibrium_with_heat_src2)
{
  int mat_size = 16;

  std::vector<double> range = {-1, 1};

  int idx_x = 0;
  int idx_y = 0;
  for (double x = -1; x < 1; x += (range[1] - range[0]) / (mat_size ))
  {
    idx_y = 0;
    for (double y = -1; y < 1; y += (range[1] - range[0]) / (mat_size))
    {
      MaterialPoint p;
      double temp;
      
      if (x <= -.5)
        temp = -x - 1;
      else if (x > .5)
        temp = -x + 1;
      else
        temp = x;
      
      p.getTemperature() = temp;
      p.getHeatRate() = 1.0;
      // compute heat source for this point
      p.getHeatSource() = (abs(abs(x) - 0.5) < 1e-6) ? std::signbit(x)*-2+1 : 0;
      p.getPosition()[0] = x;
      p.getPosition()[1] = y;
      system.addParticle(std::make_shared<MaterialPoint>(p));
      idx_y++;
    }
    idx_x++;
  }
  temperature->setDeltaT(1e-8);
  temperature->initRangeFromParticles(system);
  for (int i = 0; i < 100; i++)
  {
    temperature->compute(system);
  }

  for (auto &par : system)
  {
    double x = par.getPosition()[0];
    double equi_temp;
      if (x <= -.5)
        equi_temp = -x - 1;
      else if (x > .5)
        equi_temp = -x + 1;
      else
        equi_temp = x;
    // //verify that middle point temperature are kept at 0 d
    // if(x==0.0)
      ASSERT_NEAR(static_cast<MaterialPoint &>(par).getTemperature(), equi_temp, 1e-4);
  }
}