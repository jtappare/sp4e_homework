#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */
void ComputeTemperature::setDeltaT(Real dt) { this->dt = dt; }
void ComputeTemperature::setMassDensity(Real rho) { this->rho = rho; }
void ComputeTemperature::setSpecificHeatCapacity(Real spe_heat_capacity) { this->spe_heat_capacity = spe_heat_capacity; }
void ComputeTemperature::setNullBorder(bool null_border) { this->null_border = null_border; }

void ComputeTemperature::initRangeFromParticles(System &system)
{
   for (auto &par : system)
   {
      this->range.push_back(par.getPosition()[0]);
   }
   std::sort(this->range.begin(), this->range.end());
   auto end = std::unique(this->range.begin(), this->range.end());
   this->range.erase(end, this->range.end());
   this->dx = range[1] - range[0];
}

void ComputeTemperature::compute(System &system)
{
   int mat_size = sqrt(system.getNbParticles());
   if (!heat_src_fft_done)
   {
      Matrix<std::complex<double>> heat_src(mat_size);
      for (auto &par : system)
      {
         int i = std::round((par.getPosition()[0] - this->range[0]) / dx);
         int j = std::round((par.getPosition()[1] - this->range[0]) / dx);
         heat_src(i, j) = static_cast<MaterialPoint &>(par).getHeatSource();
      }
      this->heat_src_fft.resize(mat_size);
      this->heat_src_fft = FFT::transform(heat_src);

   //    std::cout << "heat_src: \n";
   // for (int i = 0; i < mat_size; i++)
   // {
   //    for (int j = 0; j < mat_size; j++)
   //    {
   //       std::cout << std::setprecision(4) << (heat_src(i, j)) << " ";
   //    }
   //    std::cout << std::endl;
   // }

      heat_src_fft_done = true;
   }

   Matrix<std::complex<double>> current_temp(mat_size);
   Matrix<std::complex<double>> heat_rates(mat_size);

   // get temperature of all particles in the system and store in matrix
   for (auto &par : system)
   {
      int i = std::round((par.getPosition()[0] - this->range[0]) / dx);
      int j = std::round((par.getPosition()[1] - this->range[0]) / dx);
      current_temp(i, j) = static_cast<MaterialPoint &>(par).getTemperature();
      heat_rates(i, j) = static_cast<MaterialPoint &>(par).getHeatRate();
   }

   // get volumetric heat source
   Matrix<std::complex<double>> current_temp_hat = FFT::transform(current_temp);

   Matrix<std::complex<double>> freq = FFT::computeFrequencies(mat_size);
   freq /= 1.0/M_PI;// / (M_PI*mat_size) ; //
   Matrix<std::complex<double>> dtheta_hat_dt(mat_size);

   for (int i = 0; i < mat_size; i++)
   {
      for (int j = 0; j < mat_size; j++)
      {
         dtheta_hat_dt(i, j) = 1.0 / (this->rho * this->spe_heat_capacity) * (this->heat_src_fft(i, j) - heat_rates(i, j) * current_temp_hat(i, j) * (freq(0, i) * freq(0, i) + freq(0, j) * freq(0, j)));
         // std::cout<<"this->heat_src_fft(i, j): "<<i<<" "<<j<<" "<<this->heat_src_fft(i, j)<<" "<<heat_rates(i,j)* current_temp_hat(i, j) * freq(1, i) * freq(1, i) + freq(1, j) * freq(1, j)<<std::endl;
      }
   }

   Matrix<std::complex<double>> dtheta_dt = FFT::itransform(dtheta_hat_dt);
   // apply change of temperature
   float end_en = 0;
   for (auto &par : system)
   {
      int i = std::round((par.getPosition()[0] - this->range[0]) / dx);
      int j = std::round((par.getPosition()[1] - this->range[0]) / dx);
      if (this->null_border && (!(i % (mat_size - 1)) || !(j % (mat_size - 1))))
         static_cast<MaterialPoint &>(par).getTemperature() = 0.0;
      else
         static_cast<MaterialPoint &>(par).getTemperature() = current_temp(i, j).real() + this->dt * dtheta_dt(i, j).real();
   }

   // for (int i = 0; i < mat_size; i++)
   // {
   //    for (int j = 0; j < mat_size; j++)
   //    {
   //       std::cout <<std::setprecision(2)<< current_temp(i, j) << " ";
   //    }
   //    std::cout << std::endl;
   // }
   // stuff to print content of the matrices
   // std::cout << "heat_src_fft: \n";
   // for (int i = 0; i < mat_size; i++)
   // {
   //    for (int j = 0; j < mat_size; j++)
   //    {
   //       std::cout << std::setprecision(4) << abs(this->heat_src_fft(i, j)) << " ";
   //    }
   //    std::cout << std::endl;
   // }
   // std::cout << "current_temp_hat: \n";
   // for (int i = 0; i < mat_size; i++)
   // {
   //    for (int j = 0; j < mat_size; j++)
   //    {
   //       std::cout << std::setprecision(4) << abs(current_temp_hat(i, j)) << " "; // * (freq(0, i) * freq(0, i) + freq(0, j) * freq(0, j))

   //    }
   //    std::cout << std::endl;
   // }
   // std::cout << "freq/dt: "
   //           << "\n";
   // for (int i = 0; i < mat_size; i++)
   // {
   //    for (int j = 0; j < mat_size; j++)
   //    {
   //       std::cout << std::setprecision(4) << freq(i, j) << " ";
   //    }
   //    std::cout << std::endl;
   // }
   // std::cout << "dtheta/dt: \n";
   // for (int i = 0; i < mat_size; i++)
   // {
   //    for (int j = 0; j < mat_size; j++)
   //    {
   //       std::cout << dtheta_dt(i, j) << " ";
   //    }
   //    std::cout << std::endl;
   // }
}
/* -------------------------------------------------------------------------- */
