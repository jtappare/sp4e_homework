import csv
import numpy as np
with open('temp_input.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=' ',)
    R = 0.2
    steps = 256 
    range = [-1,1]
    L = range[1]-range[0]
    for x in np.arange(range[0],range[1],(range[1]-range[0])/(steps)):
        for y in np.arange(range[0],range[1],(range[1]-range[0])/(steps)):
            t0 = 0.0
            heat_src = ((x**2+y**2)<R)*1.0
            heat_rate = 1.0
            writer.writerow([x,y,0, 0, 0, 0, 0 ,0, 0, 1 ,t0, heat_rate, heat_src ])
