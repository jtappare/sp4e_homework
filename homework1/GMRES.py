"""
Script with GMRES implementation to find the solution of Ax = b.
"""

# Import 
import numpy as np

def arnoldi(A, Q, n):
    """ Find orthonornal vector using Arnoldi method
    
    Parameters
    ----------
    A : array
        Invertible square matrix of size m x m.
    Q : array
        Matrix on which we iterate we obtain an orhogonal basic of the Krylov subspace.
    n : int
        Dimension of Krylov subspace.

    Returns
    -------
    h : array
        nth column of the Hessenberg matrix.
    q : array
        Krylov vector.

    """
    # Initialize h
    h = np.zeros((n + 2))
    
    # Generate a new candidate vector
    q = np.einsum('ij,j', A, Q[:, n])
    
    # Subtract the projections on previous vectors
    for i in np.arange(n + 1):
        h[i] = np.einsum("i,i", np.conj(q), Q[:, i])
        q = q - h[i]*Q[:, i]
     
    # Normalize the vector
    h[n + 1] = np.linalg.norm(q)
    q = q / h[n + 1]
    
    return h, q

def givens_rotation(v1, v2):
    """
    Calculate Given rotation matrix.

    Parameters
    ----------
    v1 : float
        First vector coordinate.
    v2 : float
        Second vector coordinate.

    Returns
    -------
    cs : float
        cs = cos(theta).
    sn : float
        sn = sin(theta)

    """
    # Compute vector length
    norm = np.sqrt(v1**2 + v2**2)
    
    # Compute cs and sn
    cs = v1 / norm
    sn = v2 / norm
    return cs, sn
    
def apply_rotation(h, cs, sn, n):
    """
    Apply Given rotation to H column

    Parameters
    ----------
    h : array
        The column of H on which apply the rotation.
    cs : float
        Cosine of the angle of rotation.
    sn : float
        Sine of the angle of rotation.
    n : int
        The iteration index.

    Returns
    -------
    h : array
        The vector h rotated.
    cs_n : float
        The updated cosine of the angle.
    sn_n : float
        The updated sime of the angle.

    """
    # Apply for ith column
    for i in np.arange(n):
        temp = cs[i] * h[i] + sn[i] * h[i + 1]
        h[i + 1] = -sn[i] * h[i] + cs[i] * h[i + 1]
        h[i] = temp
        
    # Update the next sin cos values for rotation
    cs_n, sn_n = givens_rotation(h[n], h[n + 1])
    
    # Eliminate H(i+1, i)
    h[n] = cs_n * h[n] + sn_n * h[n + 1]    
    h[n + 1] = 0.0
    
    return h, cs_n, sn_n

def my_GMRES(A, b, init_guess, max_it, threshold, callback=None):
    """
    Solve equation Ax = b using our implementation of the GMRES method.

    Parameters
    ----------
    A : array
        Invertible square matrix of size m x m.
    b : array
        Vector of size m.
    init_guess : array
        Initial guess for x. Must have a size m.
    max_it : int
        Maximal number of iterations.
    threshold : float
        Tolerance for convergence.
    callback : bool, optional
        Option to print intermediate solutions. The default is None.

    Returns
    -------
    x : array
        Solution array of size m..
    errors : list
        Error at each step.

    """
    # Initial vector
    r = b - np.einsum('ij,j', A, init_guess).reshape(-1,1)
    
    # Initialize x with initial guess
    x = init_guess
    
    # Compute b norm 
    b_norm = np.linalg.norm(b)
    
    # Compute the initial error
    err = np.linalg.norm(r) / b_norm
    # Store it 
    errors = [err]
    
    # Initialize array
    sn = np.zeros(max_it)
    cs = np.zeros(max_it)
    e1 = np.zeros(max_it + 1)
    e1[0] = 1
    r_norm = np.linalg.norm(r)
    Q = np.zeros((len(b), max_it + 1))
    H = np.zeros((max_it + 2, max_it))
    Q[:,0] = r.squeeze() / r_norm
    beta = r_norm * e1
    
    for n in np.arange(max_it):
        # Run arnoldi and add the produced vector to the list 
        H[:n + 2,n], Q[:, n + 1] = arnoldi(A, Q, n)
        
        # Eliminate the last element in H ith row and update the rotation 
        # matrix
        H[:n+2, n], cs[n], sn[n] = apply_rotation(H[:n+2, n], cs, sn, n)
        
        # Update the residual vector
        beta[n+1] = -sn[n]*beta[n]
        beta[n] = cs[n] * beta[n]
        err = np.abs(beta[n+1]) / b_norm
        
        # Save the error
        errors.append(err)
        
        
        #call callback for intermediate results
        if callback != None:
            y = np.einsum("ij,j", np.linalg.inv(H[:n+1, :n+1]), beta[:n+1])
            callback(x + np.einsum('ij,j', Q[:, :n+1], y))
        
        # Check if tolerance is reached 
        if err <= threshold:
            break
        
    # Calculate the result
    y = np.einsum("ij,j", np.linalg.inv(H[:n+1, :n+1]), beta[:n+1])
    x = x + np.einsum('ij,j', Q[:, :n+1], y) 
    
    return x, errors


        