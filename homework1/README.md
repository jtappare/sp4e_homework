# Homework: Generalized minimal residual method

## Usage
Launch the main script using:\
```python optimizer.py```\
The following options are available:
- options: 
  - -h, --help
    - Show help message and example usage
  - -m {BFGS,LGMRES,myGMRES}
    - The minimization method to use. Can be any combination of BFGS, LGMRES or myGMRES.
  - -p, --plot            
    - Plot intermediate steps and the quadratic function surface of the methods chosen. Only possible for bivariate quadratic functions
  - -A   
    - The coefficient of the matrix A given line by line. Should be given as a sequence of number separated by space.
  - -b
    - The coefficient of the vector b. Should be given as a sequence of number separated by space.
  - -i, --init_guess
    - The coefficient of the initial guess used by the optimizers. Should be given as a sequence of number separated by space.
  - -s, --steps           
    - Print intermediate steps methods chosen.
  - -t, --tol     
    - Tolerance for minimization
- Example of use:
  - `python optimizer.py -p -m BFGS LGMRES`
  - `python optimizer.py -p -m BFGS -A 8 1 1 3 -b 2 4`
  - `python optimizer.py -m myGMRES LGMRES -A 8 1 1 3 -b 2 4 -i -2.5 1`
  - `python optimizer.py -m myGMRES LGMRES -A 8 1 1 3 -b 2 4 -t 1e-9`
  - `python optimizer.py -m myGMRES LGMRES -A 8 1 1 3 -b 2 4 -s`

## Dependencies
- python3
- numpy
- scipy
- matplotlib
- argparse
## structure
- homework1
    - optimizer.py: The main script
    - GMRES.py: Our implementation of the GMRES algorithm


