"""
Script for homework 1. Comparison between three solver for Ax=b equation.
"""

# Imports
import argparse
import numpy as np
import scipy.optimize as spo 
import scipy.sparse as sps
import matplotlib.pyplot as plt

# Local import 
from GMRES import my_GMRES

def eq_to_solve(x,A,b):
    """ Quadratic function S to minimize.

    Parameters
    ----------
    x : array
        Vector of size n.
    A : array
        Array of size n x n.
    b : array
        Vector of size n.

    Returns
    -------
    Vector
        The value of the function S.

    """
    
    return .5 * x.T@A@x - x.T@b    

def my_minimize(functor, method, A, b, init_guess, tol=1e-9):
    """ Function tha call the solver corresponding to 'method' and return the 
    vector x that minimize the function S in 'result' and the intermediate 
    solution in 'steps'.
    
    Parameters
    ----------
    functor : TYPE
        DESCRIPTION.
    method : string
        String with the method. Can be "BFGS", "GMRES" or "myGMRES".
    A : array
        Array of size n x n.
    b : array
        Vector of size n.
    init_guess : array
        Initial guess vector of size n.
    tol : float, optional
        Tolerance for the solver. The default is 1e-9.

    Returns
    -------
    steps : array
        Intermediate solution at each iteration step. It is a vector of size
        (number of iteration, n)
    result : array
        Vector that minimalize the function. Size n.

    """
    # Initialize steps
    steps = init_guess
    
    # Function to store the steps
    def minimize_step_cb(xk):
        nonlocal steps
        # Concatenate the new steps with the older ones 
        steps =  np.vstack([steps, xk])

    # Method 'BFGS'
    if method == 'BFGS': 
        # Call the solver 
        res = spo.minimize(eq_to_solve, init_guess, args=(A,b), 
                              method='BFGS', tol=tol, 
                              callback=minimize_step_cb)
        # Store the vector that minimize the function S
        if res.success:
            result = res.x
        else:
            result = "Didn't converge"
    # Method 'GMRES'
    elif method == 'LGMRES':
        res = sps.linalg.lgmres(A, b, init_guess, tol=tol, atol=tol, 
                                   callback=minimize_step_cb)
        if res[1] == 0:
            result = res[0]
        else:
            result = "Didn't converge"
    # Method myGMRES'
    elif method == 'myGMRES':
        res = my_GMRES(A, b, init_guess, max_it=np.shape(A)[0], threshold=tol, 
                                   callback=minimize_step_cb)
        result = res[0]
    # Raise error is the method is not correct
    else:
        print("Please choose between: \n -'BFGS' \n -'LGMRES'\n -'myGMRES'")
        exit()
        
    return steps, result

def plot_steps(methods, all_steps, A, b):
    """
    Function to plot the surface and the steps towards the minimum. Can be a 
    subplot if multiple methods are provided.

    Parameters
    ----------
    methods : string or list of strings
        Method(s) to plot.
    all_steps : array or list of array
        Minimization steps for each method.
    A : array
        Array of size n x n.
    b : array
        Vector of size n.

    Returns
    -------
    None.

    """
    # Initialize figure
    fig , axs = plt.subplots(1, len(methods), subplot_kw={"projection": "3d"}, 
                             squeeze=False)

    # Plot the surface
    # Create coordinates to plot the surface
    X = np.arange(-3, 3, 0.2)
    Y = np.arange(-3, 3, 0.2)
    # Compute corresponding z
    Z = np.array([float(eq_to_solve(np.array([x,y]), A, b)) \
                  for x in X for y in Y])
    # Reshape array to 2D
    Z = Z.reshape((len(X), len(X)), order="F") 
    # Create meshgrid
    X, Y = np.meshgrid(X, Y)
    
    # Common features between subplots
    for ax in axs[0,:]:
        # Plot the surface
        ax.plot_surface(X, Y, Z, antialiased=False, cmap="winter", alpha=0.2)
        # Change 3D point of view
        ax.view_init(elev=45, azim=130)
        # Plot contours
        ax.contour3D(X, Y, Z,  colors="k", antialiased=False)
        # Set axis labels and ticks
        ax.set(xlabel='x', ylabel='y', xlim = [-3.5, 3.5], ylim = [-3.5, 3.5],zlim = [np.min(Z),np.max(Z)], 
                   xticks = [-3, -2, -1, 0, 1, 2, 3], 
                   yticks = [-3, -2, -1, 0, 1, 2, 3])
    
    # Plot iteration steps for all method 
    # Loop over the method results to plot in different subplots
    for i, steps in enumerate(all_steps):
        # Compute the z coordinates associated with each results 
        zdata = [float(eq_to_solve(steps[j,:], A, b)) \
                 for j in range(len(steps))]
        # Plot the steps
        axs[0,i].plot3D(steps[:,0], steps[:,1], zdata, "--r", marker=".")
        # Plot subplot title 
        axs[0,i].set_title(methods[i])
        
    plt.show()    

def main():
    # Parse input argument
    parser = argparse.ArgumentParser(description="This program compute the"
            " value x that minimize the equation 0.5*x.T*A*x-x.T*b. "
            "It can output the minimization result and steps used by three different methods: "
            "scipy.optimize.minimize, scipy.sparse.linalg.lgmres and our implementation of GMRES",formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='''Example of use:
    python optimizer.py -p -m BFGS LGMRES
    python optimizer.py -p -m BFGS -A 8 1 1 3 -b 2 4
    python optimizer.py -m myGMRES LGMRES -A 8 1 1 3 -b 2 4 -i -2.5 1
    python optimizer.py -m myGMRES LGMRES -A 8 1 1 3 -b 2 4 -t 1e-9
    python optimizer.py -m myGMRES LGMRES -A 8 1 1 3 -b 2 4 -s''')
    parser.add_argument('-m',dest='method', type=str, nargs='+', 
                        default= ["BFGS","LGMRES","myGMRES"],
                        choices=["BFGS","LGMRES","myGMRES"], 
                        help=("The minimization method to use. Can be any combination of "
                              "BFGS, LGMRES or myGMRES."))
    parser.add_argument('-p','--plot', action="store_true", default=False, 
                        help="Plot intermediate steps and the quadratic function surface of the "
                        "methods chosen. Only possible for bivariate quadratic functions")
    parser.add_argument('-A',type=float, nargs='+',default=[8,1,1,3], help='The coefficient of the matrix A given line by line. Should be given as'
                                                                            ' a sequence of number separated by space. ')
    parser.add_argument('-b',type=float, nargs='+', default=[2,4], help='The coefficient of the vector b. Should be given as a sequence of number'
                                                                        ' separated by space.')
    parser.add_argument('-i','--init_guess',type=float, nargs='+', help='The coefficient of the initial guess used by the optimizers.'
                                                                                        ' Should be given as a sequence of number separated by space.')
    parser.add_argument('-s','--steps', action="store_true", default=False, help="Print intermediate steps methods chosen.")
    parser.add_argument('-t','--tol', type=float, default=1e-9, help="Tolerance for minimization")
    args = parser.parse_args()

    # Define the parameter of the quadratic function we want to minimize    
    # Verify that A and b have consistent shapes and raise an error if not
    try:
        A = np.array(args.A).reshape((len(args.b),len(args.b)))
        b = np.array(args.b).reshape(-1,1)
    except ValueError:
        print(f'Dimension mismatch between A, b. b has size {len(args.b)}, therefore A should have size {len(args.b)**2}, not {len(args.A)}')
        exit()

    # Initial guess for the minimizer
    if args.init_guess == None:
        init_guess = np.zeros(len(b))
    else:
        init_guess = np.array(args.init_guess)
        if len(init_guess) != len(b):
            print(f'Dimension mismatch between b and the initial guess. b has size {len(b)} and initial guess {len(init_guess)}')
            exit()
    
    # Initialization
    steps = []
    results = []

    # Call the solver(s)
    for method in args.method:
        step, result = my_minimize(eq_to_solve, method, A, b, init_guess, tol=args.tol) 
        steps.append(step)
        results.append(result)

        #print result
        print("-------------------------------------")
        print(f"Method: {method}\nSolution: {result}")
        if(args.steps):
            print(f"Steps:\n{step}")

    
    #Plot the optimization steps
    if args.plot:
        if len(b) != 2:
            print("Graphical output only supports bivariate quadratic equations.")
        else:
            plot_steps(args.method,steps, A, b)
    
    return steps, results, A
        

if __name__ == '__main__':
    step, res, A = main()
    