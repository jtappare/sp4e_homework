import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import fmin
from functools import partial
from main import simulate 


def readPositions(planet_name, directory):
    file_list = np.sort(os.listdir(directory))
    positions = np.empty((len(file_list), 3))
    for i,filename in enumerate(file_list):
       dataframe = pd.read_csv(directory + filename, header=None, delim_whitespace=True, index_col=-1, comment="#")
       positions[i,:] = dataframe.loc[planet_name][0:3]
    return positions
       
def computeError(positions, positions_ref, duration=365):
    error = (np.sum(np.linalg.norm(positions[0:duration] - positions_ref[0:duration], axis=1)**2))
    return error

def generateInput(scale, planet_name, input_filename, output_filename):
    dataframe = pd.read_csv(input_filename, header=None, delim_whitespace=True, index_col=-1, comment="#")
    dataframe.loc[planet_name][3:6] = dataframe.loc[planet_name][3:6]*scale
    dataframe.insert(len(dataframe.columns), "planet_name", dataframe.index, True)
    dataframe.to_csv(output_filename,sep=" ",header=None,index=None)
     
def launchParticles(input,nb_steps,freq):
    simulate(nb_steps,freq,input,'planet',1)

def runAndComputeError(scale, planet_name, input_file, nb_steps, freq):
    generateInput(scale, planet_name, input_file, 'init.tmp')
    launchParticles("init.tmp", nb_steps, freq)
    positions = readPositions(planet_name, "dumps/")
    positions_ref = readPositions(planet_name, "../trajectories/")
    error = computeError(positions, positions_ref)
    return error

def callback(x, planet_name, input_file, nb_steps, freq):
    scalings.append(x[0])
    errors.append(runAndComputeError(x, planet_name, input_file, nb_steps, freq))


def main():
    input_file='../init.csv'
    planet_name = 'mercury'
    init_guess = 1
    freq = 1

    global scalings
    global errors
    scalings = [init_guess]
    errors = [runAndComputeError(init_guess, planet_name, input_file, 365, freq)]

    print("Looking for the best scaling factor...")
    error = fmin(runAndComputeError, init_guess, args=(planet_name, input_file, 365, freq), maxiter=10, callback=partial(callback, planet_name=planet_name, input_file=input_file, nb_steps=365, freq=freq))
    print("Done ! \n============================================================")
    print(f"The best scaling factor for Mercury initial velocity is {error[0]:.1f}.")

    #sort errors in order of the scalings
    errors_sorted=[x for _, x in sorted(zip(scalings, errors ))]

    # Plot the evolution of the error versus the scaling factor
    fig, ax = plt.subplots(1,1)
    ax.plot( sorted(scalings), errors_sorted, marker=".", c="k", ms=12)
    ax.set_ylabel("Error", fontsize=12)
    ax.set_xlabel("Scaling factor", fontsize=12)
    ax.set_yscale("log")
    ax.grid()
    plt.show()

if __name__ == "__main__":
    main()
        
