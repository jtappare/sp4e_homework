#include <pybind11/pybind11.h>
#include <functional>

namespace py = pybind11;

#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "csv_writer.hh"

PYBIND11_MODULE(pypart, m)
{

  m.doc() = "pybind of the Particles project";

  py::class_<ParticlesFactoryInterface>(
      m, "ParticlesFactoryInterface",
      py::dynamic_attr())
      .def("createSimulation", py::overload_cast<const std::string &, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>),
           py::arg("fname"),
           py::arg("timestep"),
           py::arg("func"), py::return_value_policy::reference)
      .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
      .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(
      m, "MaterialPointsFactory",
      py::dynamic_attr())
      .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(
      m, "PlanetsFactory",
      py::dynamic_attr())
      .def("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(
      m, "PingPongBallsFactory",
      py::dynamic_attr())
      .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);

  py::class_<Compute, std::shared_ptr<Compute>>(
      m, "Compute");

  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(
      m, "ComputeInteraction");

  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(
      m, "ComputeGravity")
      .def(py::init<>())
      .def("compute", &ComputeGravity::compute)
      .def("setG", &ComputeGravity::setG);

  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(
      m, "ComputeTemperature")
      .def(py::init<>())
      .def_property("conductivity", &ComputeTemperature::getConductivity, &ComputeTemperature::setConductivity)
      .def_property("L", &ComputeTemperature::getL, &ComputeTemperature::setL)
      .def_property("capacity", &ComputeTemperature::getCapacity, &ComputeTemperature::setCapacity)
      .def_property("density", &ComputeTemperature::getDensity, &ComputeTemperature::setDensity)
      .def_property("deltat", &ComputeTemperature::getDeltat, &ComputeTemperature::setDeltat);

  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(
      m, "ComputeVerletIntegration")
      .def(py::init<Real>())
      .def("addInteraction", &ComputeVerletIntegration::addInteraction);

  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(
      m, "CsvWriter",
      py::dynamic_attr())
      .def(py::init<const std::string &>())
      .def("write", &CsvWriter::write);

  py::class_<SystemEvolution>(
      m, "SystemEvolution")
      //.def(py::init<>())
      .def("evolve", &SystemEvolution::evolve)
      .def("addCompute", &SystemEvolution::addCompute,
           py::arg("compute"))
      .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference)
      .def("setNSteps", &SystemEvolution::setNSteps)
      .def("setDumpFreq", &SystemEvolution::setDumpFreq);
  py::class_<System>(
      m, "System");
}
