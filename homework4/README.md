# Homework - Pybind and Trajectory Optimization

## Usage

- clone submodules
  -  `git submodule update --init`
- create a build folder and move inside
  -  `mkdir build`
  -  `cd build`
- build
  -  `cmake ..`
  -  `make`
- run tests
  -  `make test`
-  create the dumps folder
   -  `mkdir dumps`
- Launch the main python script using:
  - `python main.py {nsteps} {freq} {filename} {particle_type} {timestep}`
  - example: `python main.py 365 1 ../circle_orbit.csv planet 1`
- Executing the optimization routine for the scaling of mercury initial velocity
  - `python velocity_correction.py`
	
  
## Dependencies

### Python

- python3
- numpy
- csv

### C++

- CMake
- gcc
- fftw3 (for material points simulations)



## Questions

### Question 2.2: How will you ensure that references to Compute objects type are correctly managed in the python bindings?

We can add to the class bindings that the reference to the object should use smart shared pointers.

## GIFs
Mercury before and after rescaling of velocity

  <img src="https://gitlab.epfl.ch/jtappare/gifs/-/raw/main/mercury_not_ok.gif" width="300" height="300" />
  <img src="  https://gitlab.epfl.ch/jtappare/gifs/-/raw/main/mercury_ok.gif" width="300" height="300" />

  

  
## Authors
- Roxane Ferry
- Joachim Tapparel
