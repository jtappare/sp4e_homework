"""
Script to find for each function (cubic, cos and sin) the number of iteration 
needed to get the value expected with a 1% error using Riemann serie.
"""

import subprocess
import matplotlib.pyplot as plt
import numpy as np

def plotRiemann():
    fig , axs = plt.subplots(3, 1, squeeze=False)
    
    # Integral bounds
    a = 0
    b = [1.,np.pi , np.pi/2]
    # Theoretical predictions
    theo = [0.25, 0, 1]  

    #target maximum error
    max_error = 1e-2
    
    min_N = [0,0,0]
    
    for i,func in enumerate(["cubic","cos","sin"]):
        errors = []
        N = 1
        done = False
        while(not done):
            # Call program to compute riemann series
            result = subprocess.check_output(["./build/src/hw2",f"-m riemann -func {func} -d print -n {N} -f {N} -p 6 -a {a} -b {b[i]}"])
            val = str(result).split(":")
            
            # Compute error as the difference of the riemann serie from the 
            # theoretical prediction
            err = abs(float(val[-1].split("\\")[0])-theo[i])
            errors.append(err)
            
            # If we reach the desired error, store the corresponding N
            if err < max_error: 
                min_N[i] = N
                done = True
            N += 1
            
        axs[i,0].semilogy(errors)
        axs[i,0].grid()
        axs[i,0].set_title(f"{func}; minimum N for error <1% : {min_N[i]}")
    
    axs[2,0].set_xlabel("N")
        
    for ax in axs[:,0]:
        ax.set_xlim(xmin=0)
        ax.set_ylabel("Error")
        
    fig.tight_layout()
    
    plt.show()

def main():
    plotRiemann()

if __name__ == "__main__":
    main()
