"""
Script to read and plot results written in a file (hw2, ex3, Q8).
"""
from ast import Try
import matplotlib.pyplot as plt
import argparse

def readFile(filename):
    """
    Read a file filename and store values and iterations.

    Parameters
    ----------
    filename : string
        Name of the file to read, including the extension.

    Returns
    -------
    iterations : list of int
        Iteration (N) at which values are printed.
    values : list of float
        Output values.

    """
    iterations = []
    values = []

    # filename must contain the extension 
    with open(filename, "r") as f:
        extension = filename.split(".")[1]
        if(extension == "csv"):
            sep = ","
        elif(extension == "psv"):
            sep = "|"
        else:
            sep = " "
        for line in f.readlines():
            try: 
                #arithmetic serie
                it_id, val, err, lb = line.split(sep)
            except:
                #pi serie
                it_id, val, lb = line.split(sep)

            iterations.append(int(it_id))
            values.append(float(val))
                
    return iterations, values

def printResults(iterations, values):
    """
    Print results in a figure.

    Parameters
    ----------
    iterations : list of int
        Iteration (N) at which values are printed.
    values : list of float
        Output values.

    Returns
    -------
    None.

    """
    fig, ax = plt.subplots()
    ax.plot(iterations, values, linestyle='--', marker='o')
    ax.set_xlabel("Iterations")
    ax.set_ylabel("Values")
    ax.grid()
    # Force x axis to have integer ticks 
    ax.xaxis.get_major_locator().set_params(integer=True)
    
    plt.show()

def main():
    # Parse input argument
    parser = argparse.ArgumentParser(description="This script read and plot"
            "results written in the file filename.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='''Example of use:
            python plot_results.py -f myfile''')
    parser.add_argument('filename', help="Filename with values to plot. "
                        "Should include the extension.")
    args = parser.parse_args()
    
    # iterations, values = readFile(args.filename)
    iterations, values = readFile(args.filename)
    printResults(iterations, values)

if __name__ == "__main__":
    main()