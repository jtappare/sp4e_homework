# Homework: c++ classes

## Usage
- create a build folder and move inside
  - `mkdir build`
  - `cd build`
- build 
  - `cmake ..`
  - `make`
- Launch the main script using:
  -```./src/hw2```
- The following options are available: 
  - -m {pi, arithmetic, riemann}
    - The serie to use. Can be pi, arithmetic or riemann.
  - -func {cubic, cos, sin}    
    - Function to compute using Riemann serie. Can be cubic, cos or sin. Required for -m "riemann".
  - -a
    - Lower integral bound for Riemann serie.
  - -b
    - Upper integral bound for Riemann serie.
  - -d {print, printf, write}           
    - Choose display method. Can be print, printf or write.
      - print: display in the terminal
      - printf: redirect the terminal output to the file result.tmp
      - write: write to a file. Filename must be specified with -o 
  - -n
    - Maximum number of step to compute the serie.
  - -f
    - Frequency for the display. Every step out of frequency lower than the maximum number of step will be displayed.
  - -p   
    - Precision for the display. Default: 4 digit.
  - -o
    - Name of the file to write (without extension).
  - -s {, or "|" or any character}     
    - Separator for the file. Can be any characters. Select "," for a .csv file, "|" for a .psv file or anything else for a .txt file. 
- Example of use from the build/ folder:
  - `./src/hw2  -m pi -d print -n 20 -f 2`
  - `./src/hw2  -m pi -d write -n 20 -f 2 -o myfile -s ,`
  - `./src/hw2  -m pi -d printf -n 20 -f 2`

Visualize results with the Python script plot_results. From the homework2/ folder:
- ```./build/src/hw2 -m pi -d write -n 60 -f 5 -o myfile -s "," ```
- ```python python_scripts/plot_results.py myfile.csv```

Visualize Riemann precision using the Python script riemann_precision. From the homework2/ folder:
- ```python python_scripts/riemann_precision.py```

## Dependencies
### Python
- python3
- numpy
- matplotlib
- argparse

### C++
- CMake
- gcc

## structure
- homework2/
    - src/
      - CMakeLists.txt
      - main.cc
      - compute_arithmetic.cc
      - compute_arithmetic.hh
      - compute_pi.cc
      - compute_pi.hh
      - dumper_series.cc
      - dumper_series.hh
      - print_series.cc
      - print_series.hh
      - riemann_integral.cc
      - riemann_integral.hh
      - series.cc
      - series.hh
      - write_series.cc
      - write_series.hh
    - python_scripts/
      - riemann_precision.py
      - plot_results.py
  -  CMakeLists.txt
  -  .gitignore
  -  README.md
## Questions 
- Exercice 2.1 What is the best way/strategy to divide the work among yourselves?
  - We can define together the interfaces DumperSeries and Series and then distribute the daughterclasses between us.
  - We can also split the creation of the python scripts as there is very little dependencies between them.
- Exercice 5.1 Evaluate the global complexity of your program.
  - Initially, the computing of each intermediate results used for printing/saving starts everytime from the beginning. This means that we compute $\sum_{i=1}^{\lfloor N/F\rfloor}(N-iF)$ intermediate terms, with $N$ the number of steps and $F$ the printing frequency
- Exercice 5.4 How is the complexity now ?
  - By keeping the intermediate results in memory, we only need to compute $N$ terms. This is obviously more efficient as we don't compute multiple times the exact same terms.
- Exercice 5.5 In the case you want to reduce the rounding errors over floating point operation by summing terms reversely, what is the best complexity you can achieve?
  - If we still want to print the intermediate results, we can first compute all the terms in reverse order but keep each cumulated term in memory. We can then substract each of those cumulated terms from the final result and get the intermediate values to print. We would therefore need to compute $N$ intermediate terms plus a substraction for each. The complexity doesn't increase by a lot, but the memory required does as we store all intermediates results.

## Authors
- Roxane Ferry
- Joachim Tapparel

