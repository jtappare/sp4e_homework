#ifndef PRINTSERIES_HH
#define PRINTSERIES_HH
#include "dumper_series.hh"

//! Class to print partial result of a serie in an ostream
class PrintSeries : public DumperSeries
{

public:
    PrintSeries(Series &series, unsigned int maxiter, unsigned int freq);
    ~PrintSeries(){};
    //! print the results of the serie computation in an ostream
    /*!
      \param series the serie to print
    */
    void dump(std::ostream &os = std::cout);
};

#endif