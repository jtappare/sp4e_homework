#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <memory>
#include <vector>
#include <fstream>
#include <sstream>
#include "compute_arithmetic.hh"
#include "compute_pi.hh"
#include "print_series.hh"
#include "write_series.hh"
#include "riemann_integral.hh"
#include <map>

std::map<std::string, std::string> parse_args(std::stringstream &ss)
{

    std::map<std::string, std::string> args;
    std::string param;
    std::string value;
    while (ss >> param, ss >> value)
    {
        args[param] = value;
    }
    return args;
}

int main(int argc, char **argv)
{
    auto series = std::shared_ptr<Series>();
    auto dumper = std::shared_ptr<DumperSeries>();
    std::ostream *output;
    std::ofstream output_file;
    // parse arguments
    std::stringstream ss;
    for (int i = 1; i < argc; i++)
    {
        ss << argv[i] << " ";
    }
    std::map<std::string, std::string> args = parse_args(ss);

    // Check the method
    if (args["-m"] == "pi")
    {
        auto serie_object = std::make_shared<ComputePi>();
        series = std::dynamic_pointer_cast<ComputePi>(serie_object);
    }
    else if (args["-m"] == "arithmetic")
    {
        auto serie_object = std::make_shared<ComputeArithmetic>();
        series = std::dynamic_pointer_cast<ComputeArithmetic>(serie_object);
    }
    else if (args["-m"] == "riemann")
    {
        std::function<double(double)> func;
        if (args["-func"] == "cubic")
        {
            func = [](double x) -> double
            { return std::pow(x, 3); };
    
        }
        else if (args["-func"] == "cos")
        {
            func = [](double x) -> double
            { return std::cos(x); };
        }
        else if (args["-func"] == "sin")
        {
            func = [](double x) -> double
            { return std::sin(x); };
        }
        else
        {
            std::cerr << "Parameter -func " << args["-func"] << " is not valid. Should be cubic, cos or sin" << std::endl;
            exit(EXIT_FAILURE);
        }
        float a,b; ///< bounds of integral
        try
        {
            a = stof(args["-a"]);
        }
        catch(const std::exception& e)
        {
            std::cerr << "Argument -a should be a float" << '\n';
            exit(EXIT_FAILURE);
        }
        try
        {
            b = stof(args["-b"]);
        }
        catch(const std::exception& e)
        {
            std::cerr << "Argument -b should be a float" << '\n';
            exit(EXIT_FAILURE);
        }
        
        auto serie_object = std::make_shared<RiemannIntegral>(a, b, func);
        series = std::dynamic_pointer_cast<RiemannIntegral>(serie_object);
    }
    else
    {
        std::cerr << "Argument -m " << args["-m"] << " is not valid" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Check the display
    int maxiter, freq, precision;
    try
    {
        maxiter = stoi(args["-n"]);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Argument -n " << args["-n"] << " is not valid" << std::endl;
        exit(EXIT_FAILURE);
    }
    try
    {
        freq = stoi(args["-f"]);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Argument -f " << args["-f"] << " is not valid" << std::endl;
        exit(EXIT_FAILURE);
    }
    try
    {
        precision = stoul(args["-p"]);
    }
    catch(const std::exception& e)
    {
        precision = 4;

    }
    
    if (args["-d"] == "print")
    {
        auto dump_object = std::make_shared<PrintSeries>(*series, maxiter, freq);
        dumper = std::dynamic_pointer_cast<PrintSeries>(dump_object);
        dumper->setPrecision(precision);
        output = &std::cout;
    }
    else if (args["-d"] == "printf")
    {
        auto dump_object = std::make_shared<PrintSeries>(*series, maxiter, freq);
        dumper = std::dynamic_pointer_cast<PrintSeries>(dump_object);
        dumper->setPrecision(precision);
        
        output_file.open("result.tmp");
        output = &output_file;
        std::cout << "Redirecting output to result.tmp" << std::endl;
    }
    else if (args["-d"] == "write")
    {
        auto dump_object = std::make_shared<WriteSeries>(*series, maxiter, freq, args["-o"]);
        if(args["-s"]!="")
            dump_object->setSeparator(args["-s"]);
        else
            dump_object->setSeparator(" ");
        
        dumper = std::dynamic_pointer_cast<WriteSeries>(dump_object);
    }
    else
    {
        std::cerr << "argument -d " << args["-d"] << " is not valid" << std::endl;
        exit(EXIT_FAILURE);
    }
    // Compute the serie and output result
    
    *output << *dumper;

    return EXIT_SUCCESS;
}