#include "riemann_integral.hh"

RiemannIntegral::RiemannIntegral(double a, double b, std::function<double(double)> func) : Series(), a(a), b(b), func(func)
{
}


double RiemannIntegral::computeTerm(unsigned int N)
{
    double delta_x = (b - a) / this->maxiter;
    double xi = this->a + this->current_index * delta_x;
    return func(xi) * delta_x;
}
