#ifndef SERIES_HPP
#define SERIES_HPP

#include <cmath>
//! Class to compute series
class Series
{

public:
    //! Returns the analytic prediction of the serie, if any
    virtual double getAnalyticPrediction() { return std::nan(""); };
    //! Compute the value of the serie up to N terms
    /*!
      \param N  index of the last term to accumulate
      \return The serie value at index N
    */
    virtual double compute(unsigned int N);
    void setMaxiter(unsigned int maxiter);

protected:
    unsigned int maxiter;       ///< max number of iteration
    unsigned int current_index; ///< current index of the serie
    double current_value;       ///< current value of the serie

    //! Compute a term of the serie
    /*!
      \param N  index of the term to compute
      \return The new term
    */
    virtual double computeTerm(unsigned int N) = 0;

    //! add a term to the serie
    void addTerm();
};
#endif