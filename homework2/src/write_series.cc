#include "write_series.hh"

WriteSeries::WriteSeries(Series &series, unsigned int maxiter, unsigned int freq, std::string out_filename) : DumperSeries(series, maxiter, freq), out_filename(out_filename)
{
    this->sep = " ";
}

void WriteSeries::setSeparator(std::string sep)
{
    this->sep = sep;
}

void WriteSeries::dump(std::ostream& os)
{
    std::ofstream out_file;
    std::string ext;
    if (this->sep == ",")
        ext = ".csv";
    else if (this->sep == "|")
        ext = ".psv";
    else
        ext = ".txt";
    out_file.open(this->out_filename + ext);
    this->series.setMaxiter(maxiter);
    double pred = this->series.getAnalyticPrediction();
    for (int i = 0; i <= this->maxiter / this->freq; i++)
    {
        float res = this->series.compute(i * this->freq);
        out_file << i * this->freq << this->sep <<res<< this->sep;
        if (std::isnan(pred))
        {
            out_file << "\n";
        }
        else
        {
            out_file  << std::abs(pred - res) << this->sep << "\n";
        }
    }
    out_file.close();
}