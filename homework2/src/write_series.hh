#ifndef WRITESERIES_HH
#define WRITESERIES_HH
#include "dumper_series.hh"
#include <fstream>

//! Class to write partial result of a serie in a csv, psv, or txt file.
class WriteSeries : public DumperSeries
{
private:
    std::string out_filename; ///< name of the output file
    std::string sep;          ///< separator to use between output values

public:
    WriteSeries(Series &series, unsigned int maxiter, unsigned int freq, std::string out_filename);
    ~WriteSeries(){};
    //! set the value separator
    /*!
      \param sep separator to use. "," and "|" will create csv and psv files respectively. Any other will create .txt files.
    */
    void setSeparator(std::string sep);

    //! save the results of the serie computation in the output file
    /*
        The output values are: serie element index, current value,(error with analytical value if applicable),\n
    */
    void dump(std::ostream &os);
};

#endif