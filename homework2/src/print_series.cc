#include "print_series.hh"

PrintSeries::PrintSeries(Series &series, unsigned int maxiter, unsigned int freq) : DumperSeries(series, maxiter, freq)
{
}
void PrintSeries::dump(std::ostream &os)
{
    double pred = this->series.getAnalyticPrediction();
    this->series.setMaxiter(maxiter);

    for (int i = 0; i <= this->maxiter / this->freq; i++)
    {
        float res = this->series.compute(i * this->freq);
        os << "iteration: "<<i * this->freq << ", value: " << std::setprecision(this->precision) << res;
        if (std::isnan(pred))
        {
            os << std::endl;
        }
        else
        {
            os << ", error remaining: " << std::abs(pred - res) << std::endl;
        }
    }
}