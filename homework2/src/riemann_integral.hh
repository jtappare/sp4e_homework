#ifndef RIEMANNINTEGRAL_HH
#define RIEMANNINTEGRAL_HH
#include <functional>
#include "series.hh"

//! Class to compute the Riemann series.
class RiemannIntegral : public Series
{
private:
    double a; ///< lower bound of integral
    double b; ///< upper bound of integral

    std::function<double(double)> func; ///< function for which we compute the integral
    //! Compute the value of the integral up to N terms of the sum.
    /*!
      \param N  index of the last term to accumulate
      \return The inegral value at index N
    */
    double computeTerm(unsigned int N);

public:
    RiemannIntegral(double a, double b, std::function<double(double)> func);
    ~RiemannIntegral(){};
};

#endif