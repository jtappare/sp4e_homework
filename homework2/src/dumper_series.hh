#ifndef DUMPERSERIES_HH
#define DUMPERSERIES_HH
#include <iostream>
#include <iomanip>

#include "series.hh"
//! Class to dump partial result of a serie.
class DumperSeries
{
public:
    //! dump the results of the serie computation
    /*!
      \param os std::ostream to dump the output to
    */
    virtual void dump(std::ostream &os) = 0;

    DumperSeries(Series &series, unsigned int maxiter, unsigned int freq) : series(series), maxiter(maxiter), freq(freq){};
    //! set the number of significant digit to output.
    virtual void setPrecision(unsigned int precision);

protected:
    unsigned int maxiter;   ///< number of iteration of the serie computation
    unsigned int freq;      ///< frequency at which we dump the intermediate results
    Series &series;         ///< serie to compute
    unsigned int precision; ///< number of significant digit to output
};

inline std::ostream &operator<<(std::ostream &stream, DumperSeries &_this)
{
    _this.dump(stream);
    return stream;
}
#endif