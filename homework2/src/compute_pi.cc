#include "compute_pi.hh"

double ComputePi::compute(unsigned int N)
{
    Series::compute(N);
    return std::sqrt(6.*this->current_value);
}

double ComputePi::computeTerm(unsigned int N)
{
    return 1./(1.*N*N);
}

double ComputePi::getAnalyticPrediction(){
    return M_PI;
}

