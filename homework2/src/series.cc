#include "series.hh"

double Series::compute(unsigned int N)
{
    if (this->current_index <= N)
    {
        N -= this->current_index;
    }
    else{
        this->current_value = 0.;
        this->current_index = 0;
    }

    for (int i = 0; i < N; i++)
    {
        this->addTerm();
    }
    return this->current_value;
    
}
void Series::addTerm(){
    this->current_index ++;
    this->current_value += this->computeTerm(this->current_index);
}

void Series::setMaxiter(unsigned int maxiter){
    this->maxiter = maxiter;
};

