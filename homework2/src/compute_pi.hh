#ifndef COMPUTEPI_HPP
#define COMPUTEPI_HPP

#include "series.hh"
#include <cstdlib>
#include <cmath>

//! Class to compute the series converging towards pi.
class ComputePi : public Series
{
public:
    ComputePi(){};// Constructor
    ~ComputePi(){}; // Destructor

    //! Compute a term of the serie converging to pi
    /*!
      \param N  index of the term to compute
      \return The new term
    */
    double computeTerm(unsigned int N) override;
    //! Compute the value of the serie up to N terms
    /*!
      \param N  index of the last term to accumulate
      \return The serie value at index N
    */
    double compute(unsigned int N) ;

    //! Returns the analytic prediction of Pi
    double getAnalyticPrediction() override;
};

#endif