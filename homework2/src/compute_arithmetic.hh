#ifndef COMPUTEARITHMETIC_HPP
#define COMPUTEARITHMETIC_HPP

#include "series.hh"
#include <cstdlib>

//! Class to compute the sum of integer up to maxiter. 

class ComputeArithmetic : public Series
{
public:
    ComputeArithmetic(){};// Constructor
    ~ComputeArithmetic(){}; // Destructor

    //! Compute a term of the arithmetic series
    /*!
      \param N  index of the term to compute
      \return The new term
    */
    double computeTerm(unsigned int N) override;

};

#endif